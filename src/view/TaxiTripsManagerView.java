package view;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.Camino;
import model.data_structures.Componente;
import model.data_structures.Lista;
import model.data_structures.Template;
import model.logic.TaxiTripsManager;


/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) throws Exception 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{

			case 1: // cargar informacion a procesar


				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadServices(100);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;
				
			case 2: // 1A: vertice más congestionado.
				String req1 = Controller.verticeMasCongestionado();
				System.out.println(req1);
				String[] r = req1.split("/");
				
				Template.mapearReq1(Double.parseDouble(r[0]), Double.parseDouble(r[1]));

				break;
				
			case 3: // 2A: componentes fuertemente conectados.
				Lista<Componente> req2 = Controller.darComponentesFuertementeConectados();
				for (int i = 0 ; i < req2.darLongitud(); i++)
				{
					System.out.println("Componente " + (i+1));
					System.out.println( "\tNumero de vertices: "+ req2.darElemento(i).numVertice);
					System.out.println("\tColor: " + req2.darElemento(i).color);
				}
				break;
			
			case 4: // 3A: mapa coloreado red vial.
				
				Template.mapearReq3(Controller.mapearGrafo());
				
				break;
			
			case 5: // 4A: camino costo mínimo.
				
				List<String> req4 = Controller.darCaminoCostoMinimo();
				System.out.println(req4);
				Template.mapearReq4(req4);

				break;
				
			case 6: // 5A: caminos mayor y menor duración.
				ArrayList<List<String>> req5 = Controller.caminosMayorMenorDuracion();
				System.out.println(req5);
				Template.mapearReq5Inicio(req5);
				Template.mapearReq5Fin(req5);
				break;
			
			case 7: // 6A: camino sin pagar peaje.
				
				List<String> req6= Controller.caminoSinPeajes();
				System.out.println(req6);
				Template.mapearReq6(req6);
				break;
				
			case 8:
				
				fin=true;
				sc.close();
				break;	
			}	
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 3----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar toda la informacion del sistema de un grafo generado anteriormente (Datos: LARGE - Dx: 100m) y cargar el archivo csv de las calles de Chicago");
		System.out.println("");
		System.out.println("---------------------Requerimientos----------------------");
		System.out.println("2. Encontrar el vértices más congestionado en Chicago (mayor cantidad de servicios en el).");
		System.out.println("3. Calcular los componentes fuertemente conexos presentes en el grafo.");
		System.out.println("4. General mapa coloreado de la red vial de Chicago.");
		System.out.println("5. Encontrar el camino de costo mínimo (menor distancia) dado un servicio que inicia en un punto (lat, long) y uno que finaliza en otro.");
		System.out.println("6. Encontrar los caminos de mayor y menor duracion entre dos vértices ( servicios dados por puntos de longitud y latitud) dados por el usuario.");
		System.out.println("7. Encontrar el camino donde no se pague peaje (si existe) entre dos servicios (puntos lat y long) dados por el usuario.");
		System.out.println("8. Salir.");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");
	}
}

