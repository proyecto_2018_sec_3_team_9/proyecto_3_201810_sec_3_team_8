package model.logic;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.Camino;
import model.data_structures.CaminosMinimos;
import model.data_structures.Componente;
import model.data_structures.GrafoDirigido;
import model.data_structures.Iterador;
import model.data_structures.KosarajuSharir;
import model.data_structures.Lista;
import model.data_structures.Vertice;
import model.data_structures.ArcoNoExisteException;
import model.data_structures.VerticeNoExisteException;
import model.vo.AdministradorDeCarga;
import model.vo.ArcoDistanciaTiempoValor;
import model.vo.VerticeLatLongServicios;


public class TaxiTripsManager implements ITaxiTripsManager 
{

	/**
	 * Administrador de carga.
	 */
	private AdministradorDeCarga admin;

	/**
	 * ArrayList de arreglos de Strings con las calles del csv;
	 */
	private ArrayList<String[]> calles;

	/**
	 * ArrayList de arreglos de Strings con las calles del csv por latitud y longitud;
	 */
	private ArrayList<String> latlongCalles;

	/**
	 * Tama�o del grafo
	 */
	private double size= 437160;

	@Override 
	/**
	 * Metodo a cargar todos lo servicios en un grafo usando sus coordenadas y una distancia de referencia.
	 * Los servicios son cargados en un grafo dirido
	 * @param distanciaRef - Distancia referencia a usar.
	 * @return true si los servicios fueron cargados correctamente, false de lo contrario.
	 */
	public boolean loadServices(double distanciaRef)
	{
		calles = new ArrayList<String[]>();
		latlongCalles = new ArrayList<String>();
		try {
			admin = new AdministradorDeCarga(distanciaRef);

			if(cargarVertices()&&cargarArcos())
				admin.imprimirInfo();
			else return false;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		try {
			cargarArchivoCalles();
			System.out.println("Se ha cargado correctamente el archivo de calles de Chicago");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * Metodo a cargar todos los arcos del grafo.
	 * @return true si los arcos fueron cargados correctamente, false de lo contrario.
	 */
	public boolean cargarArcos() throws Exception 
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream;
		try 
		{
			inputStream = new FileInputStream("./data/grafos/grafo-LARGE-100.0m/arcos-LARGE-100.0m.json");
		} catch (FileNotFoundException e1) {
			System.out.println("La persistencia de los arcos que desea cargar no existe.");
			return false;
		}

		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{

				// Instancia un servicio del json
				ArcoDistanciaTiempoValor nArco = gson.fromJson(reader, ArcoDistanciaTiempoValor.class);

				admin.agregarArco(nArco);

			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}

	/**
	 * Metodo a cargar todos los vertices del grafo.
	 * @return true si los vertices fueron cargados correctamente, false de lo contrario.
	 */
	public boolean cargarVertices() throws Exception
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream;
		try
		{
			inputStream = new FileInputStream("./data/grafos/grafo-LARGE-100.0m/vertices-LARGE-100.0m.json");
		} 
		catch (FileNotFoundException e1) 	
		{
			System.out.println("La persistencia de los vertices que desea cargar no existe.");
			return false;
		}
		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{
				// Instancia un servicio del json
				VerticeLatLongServicios nVertice = gson.fromJson(reader, VerticeLatLongServicios.class);

				admin.agregarVertice(nVertice);

			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}

	/**
	 * Metodo a cargar el archivo .csv de calles de Chicago.
	 * @return true si el archivo de calles fue cargado correctamente, false de lo contrario.
	 */
	public boolean cargarArchivoCalles() throws Exception
	{
		boolean cargo = false;
		String archivo = "./data/ChicagoStreets.csv";
		BufferedReader br = null;
		String linea = "";
		String separador = ";";
		int contador = 0;
		try {
			br = new BufferedReader(new FileReader(archivo));
			while ((linea = br.readLine()) != null) 
			{                
				String[] datos = linea.split(separador);
				//Imprime datos.
				//				System.out.println(datos[0] + ", " + datos[1] + ", " + datos[2] + ", " + datos[3] + ", " + datos[4] + ", " + datos[5] + "," + datos[6]);
				calles.add(datos);
				latlongCalles.add(datos[6]);
			}
			cargo = true;
			System.out.println("El tamaño de la lista de calles es:" + "" + calles.size());	
		}

		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		} finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		return cargo;	
	}

	/**
	 * Método que retorna la información del vértice más congestionado en Chicago (aquel que contiene la mayor cantidad de servicios que salen y llegan a él)
	 * @return su (latitud, longitud), total servicios que salieron y total de servicios que llegaron.
	 */
	@Override
	public String verticeMasCongestionado()
	{
		String vertex = "";

		GrafoDirigido grafo = admin.grafo();

		HashMap map = grafo.darMap();

		List<VerticeLatLongServicios> vertices = grafo.darVertices();

		int maximo = 0;
		int predecesores = 0;
		int sucesores = 0;

		Iterator iter = map.entrySet().iterator();

		while(iter.hasNext())
		{
			Map.Entry par = (Map.Entry)iter.next();
			Vertice vertice = (Vertice) par.getValue();
			int total = vertice.darNumeroPredecesores() + vertice.darNumeroSucesores();

			if(total > maximo)
			{
				maximo = total;
				vertex = (String) par.getKey();
				predecesores = vertice.darNumeroPredecesores() * 336;
				sucesores = vertice.darNumeroSucesores() * 193;
			}
		}

		System.out.println("Total de servicios: " + (predecesores + sucesores) + ", salen " + sucesores + " y llegan " + predecesores + ".");

		return vertex;
	}

	/**
	 * Método que calcula los componentes fuertemente conexos presentes en el grafo. Asignando un color a los vértices que componen un componente.
	 * @return Retorna una lista en donde en cada nodo, se tiene la información de un componente conexo (color, número de vértices que lo componen).
	 * @throws Exception 
	 */
	@Override
	public Lista darComponentesFuertementeConectados() throws Exception 
	{
		Lista<Componente> lista = new Lista<Componente>();

		lista = admin.grafo().contarComponentesConexos();

		KosarajuSharir k = new KosarajuSharir(admin.grafo(), admin.grafo());

		System.out.println(k.count());
		System.out.println(lista.darLongitud());
		return lista;
	}


	public String mapearGrafo()
	{
		GrafoDirigido grafo = admin.grafo();

		List<VerticeLatLongServicios> vertices  = grafo.darVertices();

		int total = 0;

		String graph = "";

		for(VerticeLatLongServicios actual: vertices)
		{
			System.out.println(actual.darIdServicios().size());
			System.out.println("Vertice " + actual.darId() + " tiene una densidad de " + (actual.darIdServicios().size()/size) + " y tendrá un radio de " + (actual.darIdServicios().size()/size)*100 + " metros.");
			graph += actual.darId()+":::"+(actual.darIdServicios().size()/size)+":::"+(actual.darIdServicios().size()/size)*1000+";;;";
			total += actual.darIdServicios().size();
		}

		System.out.println("TOTAL: "+total);
		return graph;
	}

	/**
	 * Método que encuentra el camino de costo mínimo de un servicio dado por dos puntos aleatorios(latitud, longitud).
	 * @return el camino a seguir, informando sus vértices, el tiempo estimado, la distancia estimada y el valor estimado a pagar.
	 */
	@Override
	public List<String> darCaminoCostoMinimo() throws Exception
	{
		List<String> ls= new ArrayList<String>();

		double tiempo = 0;
		double distancia = 0;
		double valor = 0;

		Random random = new Random();
		int numero1 = random.nextInt(latlongCalles.size());

		Random random2 = new Random();
		int numero2 = random2.nextInt(latlongCalles.size());

		String punto1 = latlongCalles.get(numero1);
		String[] punto11 = punto1.split(" ");

		System.out.println("lat1 " + punto11[1]);
		System.out.println("lon1 " + punto11[0]);

		System.out.println(" ");

		String punto2 = latlongCalles.get(numero2);
		String[] punto21 = punto2.split(" ");

		System.out.println("lat2 " + punto21[1]);
		System.out.println("lon2 " + punto21[0]);
		System.out.println("");


		VerticeLatLongServicios ver1 = null;
		try 
		{
			ver1 = admin.darVertice(Double.parseDouble(punto11[1]), Double.parseDouble(punto11[0]));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("vertice1 " + ver1.darId());

		VerticeLatLongServicios ver2 = null;
		try 
		{
			ver2 = admin.darVertice(Double.parseDouble(punto21[1]), Double.parseDouble(punto21[0]));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("vertice2 " + ver2.darId());
		System.out.println("");
		GrafoDirigido grafo = admin.grafo();

		Boolean existe = false;
		try {
			existe = grafo.hayCamino(ver1.darId(), ver2.darId());
		} catch (VerticeNoExisteException e) 
		{
			e.printStackTrace();
		}
		if(!existe)
		{
			System.out.println("No existe un camino entre los vertices.");
		}
		else
		{
			CaminosMinimos dij = null;
			try {
				dij = grafo.dijkstra(ver1.darId());
			} catch (VerticeNoExisteException e)
			{
				e.printStackTrace();
			}
			Vertice v2 = (Vertice) grafo.darMap().get(ver2.darId());

			Iterador iter = dij.darCaminoMinimo(v2);
			Iterador iter2 = dij.darCaminoMinimo(v2);
			int k = 0;
			int k2 = 0;
			Vertice  actual = null;		

			while(iter.haySiguiente())
			{
				if (k==0){
					Vertice orig = (Vertice) iter.darSiguiente();
					System.out.println(orig.darId());
					ls.add((String) orig.darId());
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ls.add((String) fin.darId());
					System.out.println(fin.darId());
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) 
					{
						e.printStackTrace();
					}

					tiempo += arco.darPromedioTiempo();
					distancia += arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
				}

				else
				{
					Vertice orig = actual;
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) {
						e.printStackTrace();
					}
					tiempo+=arco.darPromedioTiempo();
					distancia+=arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
					ls.add((String) fin.darId());
					System.out.println(fin.darId());
				}

				k++;
			}

			System.out.println("Tiempo estimado " + tiempo);
			System.out.println("Distancia estimada " + distancia);
			System.out.println("Valor estimado " + valor);

		}
		return ls;
	}

	/**
	 * Método que encuentra el camino de costo mínimo de un servicio dado por dos puntos aleatorios(latitud, longitud).
	 * @return sus vértices, el tiempo estimado, la distancia estimada y el valor estimado a pagar.
	 */
	@Override
	public ArrayList<List<String>> caminosMayorMenorDuracion()
	{
		ArrayList<List<String>>  ret= new ArrayList<List<String>>();
		List<String> ls= new ArrayList<String>();
		double tiempo = 0;
		double distancia = 0;
		double valor = 0;

		Random random = new Random();
		int numero1 = random.nextInt(latlongCalles.size());

		Random random2 = new Random();
		int numero2 = random2.nextInt(latlongCalles.size());

		String punto1 = latlongCalles.get(numero1);
		String[] punto11 = punto1.split(" ");

		System.out.println("lat1 " + punto11[1]);
		System.out.println("lon1 " + punto11[0]);

		System.out.println(" ");

		String punto2 = latlongCalles.get(numero2);
		String[] punto21 = punto2.split(" ");

		System.out.println("lat2 " + punto21[1]);
		System.out.println("lon2 " + punto21[0]);
		System.out.println("");


		VerticeLatLongServicios ver1 = null;
		try 
		{
			ver1 = admin.darVertice(Double.parseDouble(punto11[1]), Double.parseDouble(punto11[0]));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("vertice1 " + ver1.darId());

		VerticeLatLongServicios ver2 = null;
		try 
		{
			ver2 = admin.darVertice(Double.parseDouble(punto21[1]), Double.parseDouble(punto21[0]));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("vertice2 " + ver2.darId());
		System.out.println("");
		System.out.println("--------------------------------INICIO A FIN--------------------------------------");

		GrafoDirigido grafo = admin.grafo();

		Boolean existe = false;
		try {
			existe = grafo.hayCamino(ver1.darId(), ver2.darId());
		} catch (VerticeNoExisteException e) 
		{
			e.printStackTrace();
		}
		if(!existe)
		{
			System.out.println("No existe un camino entre los vertices.");
		}
		else
		{
			CaminosMinimos dij = null;
			try {
				dij = grafo.dijkstra(ver1.darId());
			} catch (VerticeNoExisteException e)
			{
				e.printStackTrace();
			}
			Vertice v2 = (Vertice) grafo.darMap().get(ver2.darId());

			Iterador iter = dij.darCaminoMinimo(v2);
			int k = 0;
			Vertice  actual = null;		

			while(iter.haySiguiente())
			{
				if (k==0)
				{
					Vertice orig = (Vertice) iter.darSiguiente();
					System.out.println(orig.darId());
					ls.add((String) orig.darId());
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ls.add((String) fin.darId());
					System.out.println(fin.darId());
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) 
					{
						e.printStackTrace();
					}

					tiempo+=arco.darPromedioTiempo();
					distancia+=arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
				}

				else
				{
					Vertice orig = actual;
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) {
						e.printStackTrace();
					}
					tiempo+=arco.darPromedioTiempo();
					distancia+=arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
					ls.add((String) fin.darId());
					System.out.println(fin.darId());
				}

				k++;
			}

			System.out.println("Tiempo estimado " + tiempo);
			System.out.println("Distancia estimada " + distancia);
			System.out.println("Valor estimado " + valor);

		}
		System.out.println("----------------------------FIN A INICIO--------------------------------------");
		List<String> ls1= new ArrayList<String>();
		double tiempo1 = 0;
		double distancia1 = 0;
		double valor1 = 0;

		Boolean existe1 = false;
		try {
			existe1 = grafo.hayCamino(ver2.darId(), ver1.darId());
		} catch (VerticeNoExisteException e) 
		{
			e.printStackTrace();
		}
		if(!existe1)
		{
			System.out.println("No existe un camino entre los vertices.");
		}
		else
		{
			CaminosMinimos dij = null;
			try {
				dij = grafo.dijkstra(ver2.darId());
			} catch (VerticeNoExisteException e)
			{
				e.printStackTrace();
			}
			Vertice v1 = (Vertice) grafo.darMap().get(ver1.darId());

			Iterador iter = dij.darCaminoMinimo(v1);

			int k2 = 0;
			Vertice  actual = null;		

			while(iter.haySiguiente())
			{
				if (k2==0)
				{
					Vertice orig = (Vertice) iter.darSiguiente();
					System.out.println(orig.darId());
					ls1.add((String) orig.darId());
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ls1.add((String) fin.darId());
					System.out.println(fin.darId());
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) 
					{
						e.printStackTrace();
					}

					tiempo1+=arco.darPromedioTiempo();
					distancia1+=arco.darPromedioDistancia();
					valor1 += arco.darPromedioValor();
				}

				else
				{
					Vertice orig = actual;
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					ArcoDistanciaTiempoValor arco = null;
					try {
						arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
					} catch (VerticeNoExisteException | ArcoNoExisteException e) {
						e.printStackTrace();
					}
					tiempo1+=arco.darPromedioTiempo();
					distancia1+=arco.darPromedioDistancia();
					valor1 += arco.darPromedioValor();
					ls1.add((String) fin.darId());
					System.out.println(fin.darId());
				}

				k2++;
			}

			System.out.println("Tiempo estimado " + tiempo1);
			System.out.println("Distancia estimada " + distancia1);
			System.out.println("Valor estimado " + valor1);

		}

		ret.add(ls);
		ret.add(ls1);
		return ret;
	}

	/**
	 * Método que encuentra caminos (si existen) en donde no se deba pagar peaje, dado un servicio con puntos de salida y llegada (latitud, longitutd).
	 * @return lista con todos los caminos sin pagar peajes ordenados de mayor a menor por costo y menor a mayor por tiempo.
	 */
	@Override
	public List<String> caminoSinPeajes()
	{	
		List<String> ls= new ArrayList<String>();
		double tiempo = 0;
		double distancia = 0;
		double valor = 0;

		Random random = new Random();
		int numero1 = random.nextInt(latlongCalles.size());

		Random random2 = new Random();
		int numero2 = random2.nextInt(latlongCalles.size());

		String punto1 = latlongCalles.get(numero1);
		String[] punto11 = punto1.split(" ");

		System.out.println("lat1 " + punto11[1]);
		System.out.println("lon1 " + punto11[0]);

		System.out.println(" ");

		String punto2 = latlongCalles.get(numero2);
		String[] punto21 = punto2.split(" ");

		System.out.println("lat2 " + punto21[1]);
		System.out.println("lon2 " + punto21[0]);
		System.out.println("");

		boolean noPeaje= false;
		boolean continuar=false;
		while(noPeaje==false)
		{
			VerticeLatLongServicios ver1 = null;
			try 
			{
				ver1 = admin.darVertice(Double.parseDouble(punto11[1]), Double.parseDouble(punto11[0]));
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("vertice1 " + ver1.darId());

			VerticeLatLongServicios ver2 = null;
			try 
			{
				ver2 = admin.darVertice(Double.parseDouble(punto21[1]), Double.parseDouble(punto21[0]));
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("vertice2 " + ver2.darId());
			System.out.println("");

			GrafoDirigido grafo = admin.grafo();

			Boolean existe = false;
			try {
				existe = grafo.hayCamino(ver1.darId(), ver2.darId());
			} catch (VerticeNoExisteException e) 
			{
				e.printStackTrace();
			}
			if(!existe)
			{
				System.out.println("No existe un camino entre los dos puntos");
				noPeaje=true;
			}
			else
			{
				CaminosMinimos dij = null;
				try {
					dij = grafo.dijkstraPeajes(ver1.darId());
				} catch (VerticeNoExisteException e)
				{
					e.printStackTrace();
				}
				Vertice v2 = (Vertice) grafo.darMap().get(ver2.darId());

				Iterador iter = dij.darCaminoMinimo(v2);
				int k = 0;
				Vertice  actual = null;		

				while(iter.haySiguiente())
				{
					if (k==0)
					{
						Vertice orig = (Vertice) iter.darSiguiente();
						System.out.println(orig.darId());
						ls.add((String) orig.darId());
						Vertice fin = (Vertice) iter.darSiguiente();
						actual = fin;
						ls.add((String) fin.darId());
						System.out.println(fin.darId());
						ArcoDistanciaTiempoValor arco = null;
						try {
							arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());
						} catch (VerticeNoExisteException | ArcoNoExisteException e) 
						{
							e.printStackTrace();
						}

						tiempo+=arco.darPromedioTiempo();
						distancia+=arco.darPromedioDistancia();
						valor += arco.darPromedioValor();
					}

					else
					{
						Vertice orig = actual;
						Vertice fin = (Vertice) iter.darSiguiente();
						actual = fin;
						ArcoDistanciaTiempoValor arco = null;
						try {
							arco = (ArcoDistanciaTiempoValor) grafo.darArco(orig.darId(), fin.darId());

						} catch (VerticeNoExisteException | ArcoNoExisteException e) {
							e.printStackTrace();
						}
						tiempo+=arco.darPromedioTiempo();
						distancia+=arco.darPromedioDistancia();
						valor += arco.darPromedioValor();
						if(arco.darPeaje()==true)
						{
							continuar=true;
						}
						ls.add((String) fin.darId());
						System.out.println(fin.darId());
					}

					k++;
				}




			}
			if(continuar==false)
			{
				noPeaje=true;	
			}
			// numero2 = random2.nextInt(listaCalles.size());
			// punto2 = listaCalles.get(numero2);

		}

		System.out.println("Tiempo estimado " + tiempo);
		System.out.println("Distancia estimada " + distancia);
		System.out.println("Valor estimado " + valor);

		return ls;
	}	
}