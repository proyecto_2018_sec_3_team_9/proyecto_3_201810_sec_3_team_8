package model.data_structures;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.Color;

import org.apache.commons.io.FileUtils;

public class Template 

{
	public static void mapearReq1(double lat, double lon)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var myLatLng2= {lat: "+lat+", lng: "+lon+"};" + 
					"var marker = new google.maps.Marker({" + 
					"    position: myLatLng2," + 
					"    map: map," + 
					"    title: 'Vertice mas congestionado'" + 
					"  });"

				+" var cityCircle = new google.maps.Circle({"
				+"strokeColor: '#FF0000',"
				+"strokeOpacity: 0.8,"
				+"strokeWeight: 2,"
				+"fillColor: '#FF0000',"
				+"fillOpacity: 0.35,"
				+"map: map,"
				+"center: {lat:"+ lat +", lng:"+lon+"},"
				+"radius: Math.sqrt(0.2) * 100"
				+"});";
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa1"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void mapearReq3(String str){
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);

			String[] obj= str.split(";;;");
			//double tot=0;
			String vId="";
			String lat="";
			String lon="";
			double densidad=0;
			double radio=0;
			String scriptTag="";
			String color="";
			//VARIABLES PARA PINTAR LINEAS
			double mayor1= 0;
			String mayor1Id="";
			double mayor2= 0;
			String mayor2Id="";
			double mayor3= 0;
			String mayor3Id="";
			double mayor4= 0;
			String mayor4Id="";
			
			String color1="";
			String color2="";
			String color3="";
			String color4="";

			for(int i=0;i<obj.length;i++)
			{
				String[] subObj= obj[i].split(":::");
				color= assignColor(i);

				for(int j=0;j<subObj.length;j++)
				{
					vId=subObj[0];

					String[]latlong= vId.split("/");

					lat= latlong[0];

					lon= latlong[1];

					densidad=Double.parseDouble(subObj[1]);
					radio=Double.parseDouble(subObj[2]);

				}
				if(i<78)
				{
					if(radio+densidad> mayor1)
					{
						mayor1= radio+densidad;
						mayor1Id= vId;
					}
					color1=color;
				}
				else if(i>78 && i<156)
				{
					if(radio+densidad> mayor2)
					{
						mayor2= radio+densidad;
						mayor2Id= vId;
					}
					color2=color;
				}
				else if(i>156 && i< 234 )
				{
					if(radio+densidad> mayor1)
					{
						mayor3= radio+densidad;
						mayor3Id= vId;
					}
					color3=color;
				}
				else
				{
					if(radio+densidad> mayor4)
					{
						mayor4= radio+densidad;
						mayor4Id= vId;
					}
					color4=color;
				}
				scriptTag += " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: ' " +color+ " ',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: ' " +color+ " ',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: "+ (radio+densidad)*50
						+"});";
			}
			scriptTag+= pintarLineas(str, mayor1Id, mayor2Id, mayor3Id, mayor4Id, color1, color2, color3, color4);

			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa3"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void mapearReq4(List<String> ls)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var flightPlanCoordinates = [";
			
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=ls.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa4"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void mapearReq5Inicio(ArrayList<List<String>> c)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			List<String> ls= c.get(0);
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=ls.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa5a"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void mapearReq5Fin(ArrayList<List<String>> c)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			List<String> ls= c.get(1);
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=ls.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa5b"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void mapearReq6(List<String> ls)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=ls.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];

				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/template/"+"mapa6"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	
	public static String assignColor(int i)
	{
		String color="";
		if(i<78)
		{
			color= getColor();
		}
		else if(i>78 && i<156)
		{
			color=getColor();
		}
		else if(i>156 && i< 234 )
		{
			color= getColor();
		}
		else{
			color=getColor();
		}
		return color;
	}
	
	public static String getColor()
	{
		Random rand= new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		Color randomColor= new Color(r,g,b);
		String hex = "#"+Integer.toHexString(randomColor.getRGB()).substring(2);	
		return hex;
	}
	
	public static String pintarLineas(String str,String mayor1,String mayor2,String mayor3,String mayor4,String color1,String color2,String color3,String color4)
	{
		String[] obj= str.split(";;;");
		String scriptTag="";
		String lat="";
		String lon="";
		String vId="";
		
		String[]latlong1= mayor1.split("/");
		String lat1= latlong1[0];
		String lon1= latlong1[1];
		
		
		String[]latlong2= mayor1.split("/");
		String lat2= latlong2[0];
		String lon2= latlong2[1];
		
		
		String[]latlong3= mayor1.split("/");
		String lat3= latlong3[0];
		String lon3= latlong3[1];
		
		
		String[]latlong4= mayor1.split("/");
		String lat4= latlong4[0];
		String lon4= latlong4[1];
		for(int i=0;i<obj.length;i++)
		{
			String[] subObj= obj[i].split(":::");

			for(int j=0;j<subObj.length;j++)
			{
				vId=subObj[0];

				String[]latlong= vId.split("/");

				lat= latlong[0];

				lon= latlong[1];

			}
			
			scriptTag+="var flightPlanCoordinates"+i+" = [";
			if(i<78)
			{

				scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
			
			
				scriptTag+= "{lat: "+lat1+","+ "lng: "+lon1+"}";
				
				scriptTag+="];"
						+ "var flightPath"+i+" = new google.maps.Polyline({"
						+ "path: flightPlanCoordinates"+i+","
						+ "geodesic: true,"
						+"strokeColor: ' "+color1 +"',"
						+"strokeOpacity: 1.0,"
						+"strokeWeight: 2"
						+"});"
						+"flightPath"+i+".setMap(map);";
			}
			
			else if(i>78 && i<156)
			{

				scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				
				
				scriptTag+= "{lat: "+lat2+","+ "lng: "+lon2+"}";
				
				scriptTag+="];"
						+ "var flightPath"+i+" = new google.maps.Polyline({"
						+ "path: flightPlanCoordinates"+i+","
						+ "geodesic: true,"
						+"strokeColor: ' "+color2 +"',"
						+"strokeOpacity: 1.0,"
						+"strokeWeight: 2"
						+"});"
						+"flightPath"+i+".setMap(map);";
			}
			else if(i>156 && i< 234 )
			{
				scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				
				
				scriptTag+= "{lat: "+lat3+","+ "lng: "+lon3+"}";	
				
				scriptTag+="];"
						+ "var flightPath"+i+" = new google.maps.Polyline({"
						+ "path: flightPlanCoordinates"+i+","
						+ "geodesic: true,"
						+"strokeColor: ' "+color3 +"',"
						+"strokeOpacity: 1.0,"
						+"strokeWeight: 2"
						+"});"
						+"flightPath"+i+".setMap(map);";
			}
			else
			{
				scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				
				
				scriptTag+= "{lat: "+lat4+","+ "lng: "+lon4+"}";	
				
				scriptTag+="];"
						+ "var flightPath"+i+" = new google.maps.Polyline({"
						+ "path: flightPlanCoordinates"+i+","
						+ "geodesic: true,"
						+"strokeColor: ' "+color4 +"',"
						+"strokeOpacity: 1.0,"
						+"strokeWeight: 2"
						+"});"
						+"flightPath"+i+".setMap(map);";
			}
			
			
		}
		return scriptTag;
	}
}
