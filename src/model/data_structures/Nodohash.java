package model.data_structures;

public class Nodohash<K extends Comparable<K>,V>
{
	K key;
	V value;
	Nodohash<K, V> next;
	Nodohash<K, V> anterior;

	public Nodohash() 
	{
		this.value=null;
		this.key=null;
	}
	public Nodohash(K key, V value)
	{
		this.key = key;
		this.value = value;
		next = null;
		anterior = null;
	}

	public K darLlave() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public Nodohash<K, V> getNext() {
		return next;
	}


	public void setNext(Nodohash<K, V> next) {
		this.next = next;
	}

	public Nodohash<K, V> getAnterior() {
		return anterior;
	}

	public void setAnterior(Nodohash<K, V> anterior) {
		this.anterior = anterior;
	}

	public void setNull() {
		this.key=null;
		this.value=null;
	}
}
