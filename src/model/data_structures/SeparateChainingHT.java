package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHT <K extends Comparable<K>, V>
{
	private int size;
	private int m;
	private DoubleLinkedListST<K, V>[] tabla;

	public SeparateChainingHT( int pTamano ) 
	{
		this.m = pTamano;
		tabla = (DoubleLinkedListST<K, V>[]) new DoubleLinkedListST[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DoubleLinkedListST<K, V>();
		}
	}

	public SeparateChainingHT(  ) 
	{
		this.m = 97;
		tabla = (DoubleLinkedListST<K, V>[]) new DoubleLinkedListST[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DoubleLinkedListST<K, V>();
		}
	}

	public int size() 
	{return size;}

	public void cambiarTamano(int nuevoTamano) throws Exception 
	{
		SeparateChainingHT<K, V> nuevo = new SeparateChainingHT<K, V>(nuevoTamano);
		for (int i = 0; i < tabla.length; i++) 
		{
			DoubleLinkedListST<K, V> lista = tabla[i];
			if(lista.getSize()!= 0)
			{

				while (lista.hasNext()) 
				{
					Nodohash<K, V> elemento = lista.next();
					nuevo.put(elemento.darLlave(), elemento.getValue());
				}
			}
		}

		this.size = nuevo.size;
		this.tabla = nuevo.tabla;
		this.m = nuevo.m;
	}

	public int darPrimoSiguiente(int i) 
	{
		int r=i;

		while(true) 
		{
			r++;
			if(esPrimo(r)) 
			{
				break;
			}
		}

		return r;
	}
	public boolean esPrimo(int n) 
	{
		for(int i=2;i<n;i++) 
		{
			if(n%i==0)
				return false;
		}
		return true;
	}

	public void put(K llave, V valor) throws Exception
	{

		if(valor == null) 
		{
			delete(llave);
		}
		else if(contiene(llave)) 
		{
			int indice = hash(llave);

			tabla[indice].putElement(llave, valor);
		}
		else
		{
			if((size / m) >= 6) 
			{
				cambiarTamano(darPrimoSiguiente(m));
			}
			int indice = hash(llave);
			tabla[indice].add(valor, llave);
			size++;
		}
	}

	public boolean contiene(K llave) throws Exception 
	{
		return tabla[hash(llave)].existElement(llave);
	}

	public V get (K llave) throws Exception 
	{
		if(llave == null)throw new Exception();
		int hashCode= hash(llave);
		return tabla[hashCode].findElement(llave);
	}



	public void delete(K llave) throws Exception 
	{	
		if(contiene(llave)) 
		{
			int posicion = hash(llave);
			tabla[posicion].delete(llave);
			size--;
		}
	}

	public int hash(K llave) throws Exception 
	{	if(llave == null)throw new Exception();
	return llave.hashCode() % m;
	}

	public int tamano() 
	{
		return size;
	}

	public Iterator<V> iterador() 
	{
		Queue<V> cola = new Queue<V>();
		for (int i = 0; i < tabla.length; i++) 
		{
			DoubleLinkedListST<K, V> lista = tabla[i];
			if(lista.getSize() !=0) 
			{
				while (lista.hasNext()) 
				{
					Nodohash<K, V> elemento = lista.next();
					cola.enqueue(elemento.getValue());
				}
			}

		}
		return cola.iterator();
	} 
	public Iterator<K> iteradork() {
		Queue<K> cola = new Queue<K>();
		for (int i = 0; i < tabla.length; i++) 
		{
			DoubleLinkedListST<K, V> lista = tabla[i];
			if(lista.getSize() !=0) 
			{
				while (lista.hasNext()) 
				{
					Nodohash<K, V> elemento = lista.next();
					cola.enqueue(elemento.darLlave());
				}
			}

		}
		return cola.iterator();
	} 
}
