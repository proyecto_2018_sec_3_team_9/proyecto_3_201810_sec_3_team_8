package model.vo;

import java.util.ArrayList;


import model.data_structures.LinkedList;
import model.data_structures.IVertice;

public class VerticeLatLongServicios implements IVertice<String>,Comparable<VerticeLatLongServicios>
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
	
    /**
	 * Constante para la serializaci�n 
	 */
	private static final long serialVersionUID = 1L;

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------	
	
	/**
     * Dato contenido por el v�rtice.
     */
	private ArrayList<String> idServicios;
    
	private double latitud;
    
	private double longitud;
    
	private ArrayList<String> sucesores;
    
	private ArrayList<String> predecesores;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------    
    
    /**
     * Constructor de la clase.
     * @param valor Dato contenido por el vértice.
     */
    public VerticeLatLongServicios( double latitud,double longitud,String idServicio )
    {
        this.latitud = latitud;
        this.longitud = longitud;
        idServicios  =new ArrayList<String>();
        sucesores = new ArrayList<String>();
        predecesores = new ArrayList<String>();
        idServicios.add(idServicio);
    }

    // -----------------------------------------------------------------
    // Métodos
    // -----------------------------------------------------------------    
    public double darLatitud()
    {
    	return latitud;
    }
    public double darLongitud()
    {
    	return longitud;
    }
    public ArrayList<String> darIdServicios()
    {
    	return idServicios;
    }
    
    public void agregarServicio(String idServicio)
    {
   
    	idServicios.add(idServicio);
    }
    
    public void agregarPre(String idServicio)
    {
   
    	predecesores.add(idServicio);
    }
    
    public void agregarSu(String idServicio)
    {
   
    	sucesores.add(idServicio);
    }
    
    public int darNumPredecesores()
    {
    	return predecesores.size();
    }
    
    public int darNumSucesores()
    {
    	return sucesores.size();
    }
	
    @Override
	public String darId() 
    {
		return latitud+"/"+longitud;
	}

	@Override
	public int compareTo(VerticeLatLongServicios arg0) 
	{
		return darId().compareTo(arg0.darId());
	}
}
