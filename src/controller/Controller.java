package controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import api.ITaxiTripsManager;
import model.data_structures.Camino;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Service;
import model.vo.Taxi;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean loadServices(double distanciaReferencia)
	{
		return manager.loadServices(distanciaReferencia);
	}
	
	public static String verticeMasCongestionado()
	{
		return manager.verticeMasCongestionado();
	}
	
	public static Lista darComponentesFuertementeConectados() throws Exception
	{
		return manager.darComponentesFuertementeConectados();
	}
	
	public static String mapearGrafo()
	{
		return manager.mapearGrafo();
	}
	
	public static List<String> darCaminoCostoMinimo() throws Exception
	{
		return manager.darCaminoCostoMinimo();
	}
	
	public static ArrayList<List<String>> caminosMayorMenorDuracion()
	{
		return manager.caminosMayorMenorDuracion();
	}
	
	public static List<String> caminoSinPeajes()
	{
		return manager.caminoSinPeajes();
	}
}

