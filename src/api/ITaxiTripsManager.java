package api;

import model.vo.Taxi;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import model.data_structures.Camino;
import model.data_structures.Lista;
import model.vo.Service;

/**
 * API basico para probar la funcionalidad de TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Metodo a cargar todos lo servicios en un grafo usando sus coordenadas y una distancia de referencia.
	 * Los servicios son cargados en un grafo dirido
	 * @param distanciaRef - Distancia referencia a usar.
	 * @return true si los servicios fueron cargados correctamente, false de lo contrario.
	 */
	public boolean loadServices(double distanciaRef);
	
	public String verticeMasCongestionado();
	
	public Lista darComponentesFuertementeConectados() throws Exception;
	
	public String mapearGrafo();
	
	public List<String> darCaminoCostoMinimo() throws Exception;
	
	public ArrayList<List<String>> caminosMayorMenorDuracion();
	
	public List caminoSinPeajes();
	
}
